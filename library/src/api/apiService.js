import axios from 'axios';

/* API ADDRESS */
const API_URL = 'http://localhost:3005/';

/* ROUTES */
async function getAllBooks(){
  const res = await axios.get(`${API_URL}books`);
  return res.data.books;
}

async function submitBook(values){
  //console.log(values);
  const res = await axios.post(`${API_URL}addbook`, values);
  return res.data.books;
}

async function updateBook(values){
  const {id} = values
  const res = await axios.put(`${API_URL}update/${id}`, {values});
  console.log(`${API_URL}update/${id}`); 
  //await axios.get(`${API_URL}books`)
  return res.data.books;  
}

async function deleteBook(id){
  await axios.delete(`${API_URL}delete/${id}`);
  const res = await axios.get(`${API_URL}books`)
  return res.data.books;
}

export {getAllBooks, submitBook, updateBook, deleteBook}