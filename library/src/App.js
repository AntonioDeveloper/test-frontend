import React from 'react';
import './App.css';
import BooksProvider from './context/books';
import {Container} from 'react-bootstrap';
import BooksBoard from './components/BooksBoard';
import Top from './components/Top';
import Bottom from './components/Bottom';


function App() {

  return (
    <BooksProvider>
      <Container fluid className="bg">
        <Top />
        <BooksBoard />    
      </Container>
    </BooksProvider>
  );
}

export default App;
