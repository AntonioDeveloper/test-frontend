import React, {createContext, useContext, useState, useEffect} from 'react';
import * as api from '../api/apiService';

/* CONTEXT FOR STORING STATE FOR THE WHOLE APP */
const BooksContext = createContext();

export default function BooksProvider({children}){
  /* LOAD DATA FROM BACKEND */ 
  const [allBooks, setAllBooks] = useState([]);
   
  useEffect(() => {
    const getBooks = async () => {
      const books = await api.getAllBooks();
      setAllBooks(books);
    };
    
    getBooks();
  }, []);
  
  
  return (
    <BooksContext.Provider value={{
      allBooks, setAllBooks
    }}>
      {children}
    </BooksContext.Provider>
  );  
}

export function useContextBooks(){
  const context = useContext(BooksContext);
  if(!context)
    throw new Error ("useContextBooks must be used within a Provider");
    const {allBooks, setAllBooks} = context;
    return {allBooks, setAllBooks};
  }
