import React, {useState} from 'react';
import Modal from 'react-modal';
import {submitBook} from '../api/apiService';
import {Button} from 'react-bootstrap';
//Modal.setAppElement('#catalog');

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    height                : '400px'
  }
};

export default function CreateRegister(props){   
  

  const persistRegister = (e) => {
      e.preventDefault();
      
      const values = {       
        title: document.getElementById('title').value,
        author: document.getElementById('author').value,
        price: document.getElementById('price').value,
      }

      /* VALIDATION OF THE FORM */
      if(values.title === ""){
        document.getElementById('warn').style.display="block";
        return;
      }

      if(values.author === ""){
        document.getElementById('warn').style.display="block";
        return;
      }

      if(values.price === 0 || values.price === "+" ||  values.price === "-"){
        document.getElementById('warn-price').style.display="block";
        return;
      }
      

      submitBook(values);
      document.form_reg.reset();
      //console.log(values);
  }

  const {open, close} = props;

 //console.log(typeof(open))

  return(
    <div>        
        <Modal
          isOpen={open}    
          onRequestClose={close}
          style={customStyles}
          contentLabel="Modal de Cadastro de Produtos"
        >
          
          <Button className="button-close" variant="danger" name="close" onClick={close}>X</Button>
          <h2 style={{margin: "20px 0"}}>Cadastro de Produto</h2>
          <span id="warn">Por favor, preencha todos os campos do formulário</span>
          <span id="warn-price">Por favor, utilize somente números</span>
                    
          <form name="form_reg" id="form-reg">
            <input type="text" name="title" id="title" placeholder="Título da obra"  required />
            <input type="text" name="author" id="author" placeholder="Autor"  required />
            <input type="number" name="price" id="price" placeholder="Preço"  required />
            
            <Button variant="success" type="submit" name="submit" onClick={(e) => persistRegister(e)}>Enviar</Button>
          </form>
        </Modal>        
      </div>
  );
}