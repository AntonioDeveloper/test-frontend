import React, {useState}  from 'react';
import {Row, Col, Button} from 'react-bootstrap';
import CreateRegister from './CreateRegister';


export default function Top(){
  const [modalIsOpen,setIsOpen] = useState(false);


  /* MODAL FUNCTIONS */
    function openModal() {
    setIsOpen(true);
  }
 
  function closeModal(){
    setIsOpen(false);
  }

  return(
    <Row className="top-row">
      <Col className="top-col">
      <Button onClick={openModal}>Cadastrar Produto</Button>
      <CreateRegister open={modalIsOpen} close={closeModal} register={openModal}/>  
      </Col>
    </Row>
  );
}