import React, {useState} from 'react';
import {Button} from 'react-bootstrap';
import Modal from 'react-modal';
import {updateBook} from '../api/apiService';
//Modal.setAppElement('#catalog');

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    height                : '400px'
  }
};

export default function ReactModal(props){   
 
  const persistUpdate = (e) => {
      e.preventDefault();
      
      const values = {
        id: document.getElementById('id').value,
        title: document.getElementById('title').value,
        author: document.getElementById('author').value,
        price: document.getElementById('price').value,
      }

        /* VALIDATION OF THE FORM */
        /* if(values.id === "" || typeof(values.id) !== "number"){
          document.getElementById('warn').style.display="block";
          return;
        } */

        if(values.title === ""){
          document.getElementById('warn').style.display="block";
          return;
        }
  
        if(values.author === ""){
          document.getElementById('warn').style.display="block";
          return;
        }
  
        /* if(values.price === 0 || typeof(values.price) !== "number"){
          document.getElementById('warn-price').style.display="block";
          return;
        } */

      updateBook(values);
      //console.log(values);
  }

  const {open, opened, close} = props;

 //console.log(typeof(open))

  return(
    <div>
        {/* <button onClick={openModal}>Open Modal</button> */}
        <Modal
          isOpen={open}
          onAfterOpen={opened}
          onRequestClose={close}
          style={customStyles}
          contentLabel="Modal de Atualização de Produtos"
        >
 
          <Button className="button-close" variant="danger" name="close" onClick={close}>X</Button>
          <h2 style={{margin: "20px 0"}}>Atualização de Produto</h2>
          
          <form id="form-id">
            <input type="number" name="identification" id="id" placeholder="Id do produto" required />
            <input type="text" name="title" id="title" placeholder="Título da obra"  required />
            <input type="text" name="author" id="author" placeholder="Autor"  required />
            <input type="number" name="price" id="price" placeholder="Preço"  required />
            
            <Button className="button" variant="success" type="submit" name="submit" onClick={persistUpdate}>Enviar</Button>
          </form>
        </Modal>
      </div>
  );
}