import React from 'react';
import {Row, Col, Form, Button} from 'react-bootstrap';
import {submitBook, updateBook} from '../api/apiService';

export default function Register(){

  const handleSubmit = (e) => {    
    e.preventDefault();

    const values = {
      title: document.getElementById('title').value,
      author: document.getElementById('author').value,
      price: document.getElementById('price').value,
    }
    submitBook(values);
    //console.log(values);
  }

  const handleUpdate = (e) => {
    e.preventDefault();
    
    const values = {
      id: document.getElementById('id').value,
      title: document.getElementById('title2').value,
      author: document.getElementById('author2').value,
      price: document.getElementById('price2').value,
    }

    updateBook(values);
    //console.log(values);
}

  return (
    <Row>
      <Col>
      <Form>
          <Form.Group id="register">
            <Form.Label>Título</Form.Label>
            <Form.Control type="text" placeholder="Nome da obra" id="title"/>

            <Form.Label>Autor</Form.Label>
            <Form.Control type="text" placeholder="Nome" id="author"/>

            <Form.Label>Preço</Form.Label>
            <Form.Control type="price" placeholder="Valor" id="price"/>
            
          </Form.Group>
          <Button variant="primary" type="submit" onClick={(e) => handleSubmit(e)}>
            Cadastrar
          </Button>

          <Form.Group id="update">
            <Form.Label>Id</Form.Label>
            <Form.Control type="number" placeholder="Id do produto" id="id"/>
            
            <Form.Label>Título</Form.Label>
            <Form.Control type="text" placeholder="Nome da obra" id="title2"/>

            <Form.Label>Autor</Form.Label>
            <Form.Control type="text" placeholder="Nome" id="author2"/>

            <Form.Label>Preço</Form.Label>
            <Form.Control type="number" placeholder="Valor" id="price2"/>
            
          </Form.Group>

          <Button variant="primary" type="submit" onClick={(e) => handleUpdate(e)}>
            Atualizar
          </Button>

        </Form>
      </Col>
    </Row>
  );
}