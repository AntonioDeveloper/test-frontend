import React, {useState} from 'react';
import {Table, Dropdown} from 'react-bootstrap';
import {useContextBooks} from '../context/books';
import {deleteBook} from '../api/apiService';
import ReactModal from './ReactModal';
import '../App.css';

export default function BooksBoard(){

  const {allBooks} = useContextBooks();
  const [modalIsOpen,setIsOpen] = useState(false);

  /* BUTTON FUNCTIONS */
  const handleDelete=(e)=>{
    deleteBook(parseInt(e))
  }

  const handleUpdate=(e)=>{    
    openModal();    
  }

  function openModal() {
    setIsOpen(true);
  }
 
  function closeModal(){
    setIsOpen(false);
  }

  return(
    <div className="table">
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>Autor</th>
            <th>Preço</th>
          </tr>        
        </thead>
        <tbody>
          {allBooks.map((book, id) => {
            return(
              <tr key={id}>
                <th scope="row">{book.id}</th>
                <td>{book.title}</td>
                <td>{book.author}</td>
                <td>R$ {book.price}</td>

                {/* BUTTON OPÇÕES*/}
                <td>
                  <Dropdown > 
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      Opções
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item eventKey={book.id} name="edit" onSelect={function(e) {handleUpdate(e)}}>Editar</Dropdown.Item>
                      <Dropdown.Item eventKey={book.id} name="exclude" onSelect={function(e) {handleDelete(e)}}>Excluir</Dropdown.Item>                    
                    </Dropdown.Menu>
                  </Dropdown>
                </td>
              </tr>
            )
          })}
        </tbody>
      </Table>   
        
      <ReactModal open={modalIsOpen} close={closeModal} update={handleUpdate}/>     
    </div>
  );
}

/* onSelect={function(e) {handleUpdate(e)}} */