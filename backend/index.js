/* BUILDING THE BACKEND SERVER */
const express = require('express');
const app = express();
const fs = require('fs');
const cors = require('cors');

app.use(express.json());
app.use(cors(
  //origin
  'http://localhost:3000'
));

app.listen(3005, function(){
  console.log("api started");
})

/* ROUTES */
/* get all books */
app.get('/books', (_, res) => {
  fs.readFile('books.json', 'utf-8', (err, data) => {
    if(!err){
      res.send(data);
    } else{
      res.send("Não foi possível obter dados.")
    }
  })
});


/* add new book */

app.post('/addbook', (req, res) => {
  let book = req.body;
  fs.readFile('books.json', 'utf-8', (err, data) => {
    if(!err){
      try{
        let json = JSON.parse(data);
        let id = json.nextId.num++; 
        book = {id, ...book};
        
        json.books.push(book);
       
        fs.writeFile('books.json', JSON.stringify(json), err => {         
          if(err){
            res.send("Não foi possível obter dados.");             
          } else {
            res.send('Livro cadastrado com sucesso!');          
          }
        });
      } catch (err){
        res.status(400).send(err.message);
      }
    } else {
      res.status(400).send(err.message);
    }
  })
})

/* delete a book */
app.delete('/delete/:id',(req, res) => {  
  fs.readFile('books.json', 'utf-8', (err, data) => {
    if(!err){
      try{
        let json = JSON.parse(data);   
        
      const newBooks = json.books.filter(book => book.id !== parseInt(req.params.id, 10))
      json.books = newBooks;
             
          fs.writeFile('books.json', JSON.stringify(json), err => {
            if(!err){
              console.log("Livro deletado com sucesso");
            } else{
              console.log("Não foi possível deletar livro.");
            }
          });  
          res.end(); 
      } catch(err){
        res.send(400).send("Não foi possível deletar livro.");        
      } 
    } else {
      res.send(400).send("Não foi possível deletar livro.");
    }
  });
});

/* update book */
app.put('/update/:id', (req, res) => {
  let updatedBook = req.body;
  console.log(req.params.id);
  fs.readFile('books.json', 'utf-8', (err, data) => {
    if(!err){
      try{
        let json = JSON.parse(data);
        let index = json.books.filter(book => book.id === req.params.id);
        /*  if(index === -1){
          throw new Error ('Livro não cadastrado, verifique.');
        } */
        
        /* if(updatedBook.title){
          json.books[index].title = updatedBook.title;
        } */
        
        /*     switch(updatedBook){
          case updatedBook.title:
            json.books[index].title = updatedBook.title;
            break;
            case updatedBook.author:
              json.books[index].author = updatedBook.author;
              break;
              case updatedBook.price:
                json.books[index].price = updatedBook.price;
                break;
              }  
              */
             index = updatedBook.values;
             json.books[req.params.id] = index
             console.log(index);


             
             fs.writeFile('books.json', JSON.stringify(json), (err) => {
          if(err){
            res.send("Erro na atualização");
          } else {
            res.send(updatedBook.title);
          }
        });
      } catch (err){        
        res.status(400).send(err.response);
        console.log(err.response);
      }
    } else {
      res.status(400).send("Erro na leitura");
    }
  })
})