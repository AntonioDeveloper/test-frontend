const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('backend/books.json');
const middlewares = jsonServer.defaults();

const port = 3003;

server.use(router);
server.use(middlewares);
if(!router){
  console.log("Error");
} else {
  console.log("Ok");
}

server.listen(port, ()=>{
  console.log(`JSON is running on port 3003`);
});